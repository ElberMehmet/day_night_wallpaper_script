#!/bin/bash
while :
do
HOUR=$(date +%H%M)
case $HOUR in
0[0-1][0-2][0-9]) feh --bg-scale ~/wall/wall1.jpeg;; #0000 - 0129
0[1-2][3-5][0-9]) feh --bg-scale ~/wall/wall2.jpeg;; #0130 - 0259
0[3-4][0-2][0-9]) feh --bg-scale ~/wall/wall3.jpeg;; #0300 - 0429
0[4-5][3-5][0-9]) feh --bg-scale ~/wall/wall4.jpeg;; #0430 - 0559
0[6-7][0-2][0-9]) feh --bg-scale ~/wall/wall5.jpeg;; #0600 - 0729
0[7-8][3-5][0-9]) feh --bg-scale ~/wall/wall6.jpeg;; #0730 - 0859
09[0-5][0-9] | 10[0-2][0-9]) feh --bg-scale ~/wall/wall7.jpeg;; #0900 - 1029
1[0-1][3-5][0-9]) feh --bg-scale ~/wall/wall8.jpeg;; #1030 - 1159
1[2-3][0-2][0-9]) feh --bg-scale ~/wall/wall9.jpeg;; #1200 - 1329
1[3-4][3-5][0-9]) feh --bg-scale ~/wall/wall10.jpeg;; #1330 - 1459
1[5-6][0-2][0-9]) feh --bg-scale ~/wall/wall11.jpeg;; #1500 - 1629
1[6-7][3-5][0-9]) feh --bg-scale ~/wall/wall12.jpeg;; #1630 - 1759
1[8-9][0-2][0-9]) feh --bg-scale ~/wall/wall13.jpeg;; #1800 - 1929
19[0-5][0-9] | 20[0-5][0-9]) feh --bg-scale ~/wall/wall14.jpeg;; #1930 - 2059
2[1-2][0-2][0-9]) feh --bg-scale ~/wall/wall15.jpeg;; #2100 - 2229
23[3-5][0-9]) feh --bg-scale ~/wall/wall16.jpeg #2230 - 2359
esac
sleep 1
done
